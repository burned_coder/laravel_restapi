<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request as Req;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getAllData($id = -1){
        if($id > -1){
            $data = DB::table("usuarios")->Where('id', $id)->get();                     
        }else{
            $data = DB::table("usuarios")->get(); 
        }
        return $data;
    }

    public function getByMail(Req $req){
        $data = DB::table("usuarios")->Where('email', $req->input('email'))->get();       
        return $data;
    }

    public function insert(Req $req){   
        $filas = DB::table('usuarios')->insert([
            'nombre' => $req->input('nombre'),
            'apellidos' => $req->input('apellidos'),            
            'fecha_nacimiento' => $req->input('fecha_nacimiento'),
            'email' => $req->input('email'),
            'ciudad' => $req->input('ciudad')
        ]);

        if($filas < 1){
            $retorno = "Error. Se han agregado ".$filas." filas";
        }else{
            $retorno = "Se han añadido ".$filas." filas";
        }

        return $retorno;
    }

    public function delete($id){
        $filas = DB::table('usuarios')->where('id', $id)->delete();
        return "Filas eliminadas: ".$filas;
    }

    public function update(Req $req){    
        $filas = DB::table('usuarios')->where('id', $req->input('id'))->update([
            'nombre' => $req->input('nombre'),
            'apellidos' => $req->input('apellidos'),            
            'fecha_nacimiento' => $req->input('fecha_nacimiento'),
            'email' => $req->input('email'),
            'ciudad' => $req->input('ciudad')
        ]);

        if($filas < 1){
            $retorno = "Error. Se han cambiado ".$filas." filas";
        }else{
            $retorno = "Se han modificado ".$filas." filas";
        }

        return $retorno;
    }
}