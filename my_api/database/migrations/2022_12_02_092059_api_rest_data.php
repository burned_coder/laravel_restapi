<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('fecha_nacimiento');
            $table->string('email');
            $table->string('ciudad');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('usuarios', function (Blueprint $table) {
            Schema::drop('usuarios');
        });
    }
};
