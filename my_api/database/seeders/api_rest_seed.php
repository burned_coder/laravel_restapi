<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class api_rest_seed extends Seeder
{
    public function run()
    {
        DB::table('usuarios')->insert([            
            "nombre" => "Manolo",
            "apellidos" => "Rod",           
            "fecha_nacimiento" => "1985-05-25",
            "email" => "manolo@gmail.com",
            "ciudad" => "Granada"
        ]);

        DB::table('usuarios')->insert([            
            "nombre" => "Juan",
            "apellidos" => "Pescanova",            
            "fecha_nacimiento" => "1990-06-30",
            "email" => "juan@gmail.com",
            "ciudad" => "Madrid"
        ]);

        DB::table('usuarios')->insert([            
            "nombre" => "Adela",
            "apellidos" => "Roquefor",            
            "fecha_nacimiento" => "1980-04-03",
            "email" => "adela@gmail.com",
            "ciudad" => "Badajoz"
        ]);
    }
}
