<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/v1/{id?}/', 'App\Http\Controllers\Controller@getAllData');
Route::post('/v1/get_mail/', 'App\Http\Controllers\Controller@getByMail');
Route::post('/v1/', 'App\Http\Controllers\Controller@insert');
Route::delete('/v1/{id?}', 'App\Http\Controllers\Controller@delete');
Route::put('/v1/', 'App\Http\Controllers\Controller@update');
